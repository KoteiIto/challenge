$( document ).ready(function(){
	
	//プロパティクラスを定義
	function GameProperty(){
		this.stageNamePath = "./json/stage_name.json";
		this.stagePath = "./json/stage/";
		this.propertyPath = "./json/property/";
		this.filePath = "stage1.json";
		this.localStrageStageKey = "soukobanSaveStage";
		this.localStragePropertyKey = "soukobanSaveProperty";
	}
	
	//方向クラスを定義
	function Vector3(z,y,x){
		this.x = Number(x);
		this.y = Number(y);
		this.z = Number(z);
	}
	
	//グローバル変数
	var menuIndex = 0;
	var stageNameIndex = 0;
	var difficultyIndex = 0;
	var isLoadSave = false;
	var stageNameList = [];
	var gameMode = "difficulty0";
	var gameProperty = new GameProperty();
	var stageData = null;
	var propertyData = null;
	
	$.getJSON(gameProperty.stageNamePath,function(data){
		stageNameList = data;
		stageData = JSON.parse(localStorage.getItem(gameProperty.localStrageStageKey));
		propertyData = JSON.parse(localStorage.getItem(gameProperty.localStragePropertyKey));
		if(stageData != null){
			stageNameList.push("save");
		}
		for(var i = 0; i < stageNameList.length;i++){
			var $stageName = $("<p></p>");
			$stageName
			.text(stageNameList[i])
			.attr("id","stage-name"+i)
			.appendTo("#menu1");
			
			if(i == 0){
				$stageName.addClass("selected");
			}
		}
	});
	
	$('html').keydown(function(e){
		if(e.keyCode == 13){
			nextMenu();
			return;
		}
		
		if(menuIndex == 1) {
			$("#stage-name"+stageNameIndex)
			.removeClass();
			switch(e.keyCode){
				case 38: // 上
					if(stageNameIndex > 0){
						stageNameIndex--;
					}		
					break;	
				case 40: // 下
					if(stageNameIndex < stageNameList.length - 1){
						stageNameIndex++;
					}		
					break;
				default:
					return;
				}
			$("#stage-name"+stageNameIndex)
			.addClass("selected");
		}else if(menuIndex == 2){
			$("#difficulty"+difficultyIndex)
			.removeClass();
			switch(e.keyCode){
				case 38: // 上
					difficultyIndex = 0;
					break;
				case 40: // 下
					difficultyIndex = 1;
					break;
				default:
					return;
				}
			$("#difficulty"+difficultyIndex)
			.addClass("selected");
		}
	});　
		
	function nextMenu(){
		$("#menu"+menuIndex)
		.fadeOut(100,function(){
			menuIndex++;
			if(menuIndex == 3){
				var text = $("#menu1 .selected").text();
				if(text == "save"){
					isLoadSave = true;
				}
				gameProperty.filePath = text + ".json";
				gameMode = $("#menu2 .selected").attr("id");
				$("#top").css("display","none");
				$("#map").css("display","block");
				stageData = null;
				propertyData = null;
				gameStart();
			}
			
			$("#menu"+menuIndex)
			.fadeIn(100);
		});
	}
	
	var gameStart = function(){
		
		var pause = false;
		var playerImageState = "forward";
		var playerMoveState = 0;
		var frame = 0;
		var playerClass = playerImageState + playerMoveState;
		var timeLimit = 60;//Defalt
		var turnLimit = 30;//Defalt
		var stageDataList = [];
		var stageDataListIndex = 0;
		var playerImageStateList = ["playerClass"];
		var playerImageStateListIndex = 0;
		
		var dataPath = gameProperty.stagePath + gameProperty.filePath;
		
		if(gameMode == "difficulty0"){
			$("#time-limit").css("display", "none");
			$("#turn-limit").css("display", "none");
		}
		
		if(!isLoadSave){
			//DOMを構築する
			$.getJSON(dataPath,function(data){
				stageData = data;	
				stageDataList[stageDataListIndex] = stageData;	
				updateDom(0);
				updateDom(1);
			});
			
			dataPath = gameProperty.propertyPath + gameProperty.filePath;
			//プロパティからデータを読み込む
			$.getJSON(dataPath,function(data){
				propertyData = data;
				timeLimit = propertyData.timeLimit;
				turnLimit = propertyData.turnLimit;
			});
			
			$(".player").addClass(playerClass);	
		}else{
			stageData = JSON.parse(localStorage.getItem(gameProperty.localStrageStageKey));
			propertyData = JSON.parse(localStorage.getItem(gameProperty.localStragePropertyKey));
			stageDataList[stageDataListIndex] = stageData;
			updateDom(0);
			updateDom(1);
			$(".player").addClass(playerClass);	
			timeLimit = propertyData.timeLimit;
			turnLimit = propertyData.turnLimit;
		}

		
		function updateDom(z){
			//レイヤzのDOMを構築する
			$("#map_layer" + z)
			.find("div")
			.remove();
			for(var i = 0; i < stageData[1].length;i++){
				var $row = $("<div></div>");
				$row
				.attr("id", "row" + i)
				.appendTo("#map_layer" + z);
				for(var j = 0; j < stageData[z][i].length;j++){
					var $col = $("<div></div>");
					$col
					.attr("id", "col" + j)
					.appendTo($row)
					.addClass(getColClass(stageData[z][i][j]));
				}	
			}
		}
		
		//各オブジェクトのクラス名を取得する
		function getColClass(name){
			var className;
			switch(name){
				case "#":
					className = "block";
					break;
				case "p":
					className = "player";
					break;
				case "o":
					className = "ball";
					break;
				case " ":
					className = "blank";
					break;
				case ".":
					className = "goal";
					break;
			}
			return className;
		};
			
		//キーボード入力に対して移動のイベントリスナーを登録する
	    $('html').keyup(function(e){
			if(pause){
				return;
			}
			var direction;
			var $player = $(".player");
			switch(e.which){
				case 39: // 右
					direction = new Vector3(0, 0, -1);
					playerImageState = "right";				
					break;
			
				case 37: // 左
					direction = new Vector3(0, 0, 1);
					playerImageState = "left";				
					break;
			
				case 38: // 上
					direction = new Vector3(0, 1, 0);
					playerImageState = "forward";				
					break;
			
				case 40: // 下
					direction = new Vector3(0, -1, 0);
					playerImageState = "back";				
					break;
				default:
					return;
			}
			copyStageData();
			playerClass = playerImageState + playerMoveState;
			updateDom(1);
			$(".player").addClass('player ' + playerClass);
			movePlayer(direction);
			turnLimit = turnLimit - 1;
			countTurn();
	    });
		
		function copyStageData() {
			var copyedStageData = $.extend(true, [], stageDataList[stageDataListIndex]);
			stageDataListIndex++;
			playerImageStateListIndex++;
			stageDataList[stageDataListIndex] = copyedStageData;
			playerImageStateList[playerImageStateListIndex] = playerImageState;
			stageData = copyedStageData;
		}
		
		$('html').keydown(function(e){
			if(pause){
				return;
			}
			switch(e.keyCode){
				case 82: // reset
					reset();					
					break;
			
				case 85: // undo
					undo();			
					break;
				case 80 ://enter
					save();
					break;
			}
		});　
		
		function undo(){
			if(stageDataListIndex > 0){
				stageDataListIndex--;
			}
			if(playerImageStateListIndex > 0){
				playerImageStateListIndex--;
			}
			stageData = stageDataList[stageDataListIndex];
			playerImageState = playerImageStateList[playerImageStateListIndex];
			updateDom(1);
		}
		
		function reset(){
			stageDataListIndex = 0;
			playerImageStateListIndex = 0;
			turnLimit = propertyData.turnLimit;
			stageData = stageDataList[stageDataListIndex];
			playerImageState = playerImageStateList[playerImageStateListIndex];
			updateDom(1);
			countTurn();
		}
		
		function save(){
			pause = true;
			var hasSave = false;
			for(var i = 0; i < stageNameList.length;i++){
				if(stageNameList[i] == "save"){
					hasSave = true;
					break;
				}
			}
			if(!hasSave){
				stageNameList.push(gameProperty.saveFileName);
			}
			stageData = stageDataList[stageDataListIndex];
			propertyData.timeLimit = timeLimit;
			propertyData.turnLimit = turnLimit;
			
			localStorage.setItem(gameProperty.localStrageStageKey, JSON.stringify(stageData));
			localStorage.setItem(gameProperty.localStragePropertyKey, JSON.stringify(propertyData));
			
			location.reload();
		}
		
		//指定方向へプレイヤーを移動させる
		function movePlayer(direction){
			var x = $(".player")
					.attr("id")
					.replace("col", "");
			
			var y = $(".player")
					.parent()
					.attr("id")
					.replace("row", "");
					
			var playerPosition = new Vector3(1, y, x);
			var power = 2;
			canMoveNext(playerPosition, direction, power);
			$(".player").addClass(playerClass);		
			if(checkClear()){
				pause = true;
				$("#message_main")
				.addClass("clear")
				.fadeIn(800)
				.fadeOut(2000,function(){
					location.reload();
				});
			}		
		};
		
		//指定方向に移動可能か判定する
		function getcanMove(nextClassName, nextPosition, direction, power){ 
			var canMove = false; 
			if(--power < 0){
				return canMove;
			}
			switch(nextClassName){
				case "block":
					break;
				case "player":
					break;
				case "ball":
					canMove = canMoveNext(nextPosition, direction, power);
					break;
				case "blank":
					canMove = true;
					break;
				case "goal":
					canMove = true;
					break;
			}
			return canMove;
		}
		
		//ボールが移動方向へ移動できるか判定し、できる場合は移動して正を返す
		function canMoveNext(thisPosition, direction, power){
			var nextPosition = new Vector3(thisPosition.z - direction.z, thisPosition.y - direction.y, thisPosition.x - direction.x);
			var nextName = stageData[nextPosition.z][nextPosition.y][nextPosition.x];
			var nextClassName = getColClass(nextName);	
			if(getcanMove(nextClassName, nextPosition, direction, power)){
				nextName = stageData[nextPosition.z][nextPosition.y][nextPosition.x];
				stageData[nextPosition.z][nextPosition.y][nextPosition.x] = stageData[thisPosition.z][thisPosition.y][thisPosition.x];
				stageData[thisPosition.z][thisPosition.y][thisPosition.x] = nextName;
				updateDom(1);
				return true;
			}
		}
	
		//クリア判定
		function checkClear(){
			for(var i = 0;i < stageData[0].length;i++){
				for(var j = 0;j < stageData[0][i].length;j++){
					if(getColClass(stageData[0][i][j]) == "goal"){
						if(getColClass(stageData[1][i][j]) == "ball"){
							
						}else{
							return false;
						}
					} 
				}
			}
			return true;
		}
		
		//残りターンを数える
		function countTurn(){
			if(turnLimit < 0 && gameMode != "difficulty0"){
				pause = true;
				$("#message_main")
				.css("background-image","url(./img/text2.png)")
				.addClass("timeup")
				.fadeIn(800)
				.fadeOut(2000,function(){
					location.reload();
				});
			}else{
				var $turnLimit = $("#turn-limit");	
				$turnLimit
				.find("span")
				.remove();
				
				$("<span></span>")
				.text("Turn : " + turnLimit)
				.appendTo($turnLimit);
			}
		}
		
		//キャラのアニメーション
	    setInterval(function(){
	        frame++;
			if(frame % 5 == 0){
				playerMoveState = (playerMoveState + 1) % 3;
				playerClass = playerImageState + playerMoveState;
				updateDom(1);
				$(".player").addClass('player ' + playerClass);
			}
			$("#map_layer2").css("background-position", 5*frame + "px 0px");
	    },100);
		
		//タイム管理
		setInterval(function(){
			if(timeLimit < 1 && gameMode != "difficulty0"){
				pause = true;
				$("#message_main")
				.css("background-image","url(./img/text2.png)")
				.addClass("timeup")
				.fadeIn(800)
				.fadeOut(2000,function(){
					location.reload();
				});
			}else{
		        timeLimit = timeLimit - 1;
				var $timeLimit = $("#time-limit");	
				$timeLimit
				.find("span")
				.remove();
				
				$("<span></span>")
				.text("Time : " + timeLimit)
				.appendTo($timeLimit);
				countTurn();
			}
	    },1000);
	};

});