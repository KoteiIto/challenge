#チャレンジ問題

##前準備(MacPC側)
###Vagrant
1. mkdir vagrant_centos7
2. cd vagrant_centos7
3. vagrant box add centos7  https://f0fff3908f081cb6461b407be80daf97f07ac418.googledrive.com/host/0BwtuV7VyVTSkUG1PM3pCeDJ4dVE/centos7.box
4. vagrant init centos7
5. vi Vagrantfile
config.vm.network "forwarded_port", guest: 80, host: 8080 をコメントアウト
config.vm.provider "virtualbox" do |vb|　をコメントアウト
vb.memory = "1024"　を　"2048"に変更してコメントアウト
end　をコメントアウト
6. vagrant plugin install vagrant-vbguest
7. vangant up
8. vagrant ssh

##前準備(Centos側)

###パッケージをアップデート
1. sudo yum update -y

###セキュリティを一時的にを止める
1. sudo systemctl stop firewalld
2. sudo setenforce 0
3. sudo vi /etc/sysconfig/selinux
   SELINUX=enforcing
        ↓
   SELINUX=disabled

###Apacheをインストール
1. sudo yum install httpd httpd-devel -y
2. sudo systemctl enable httpd
3. sudo systemctl start httpd

###tomcatをインストール
1. sudo yum install tomcat -y
2. sudo systemctl enable tomcat
3. sudo systemctl start tomcat

###mariaDBをインストール
1. sudo yum install mariadb-server mariadb-devel -y
2. sudo vi /etc/my.cnf.d/server.cnf
default-character-set=utf8を    [mysql]の下に追加
3. sudo systemctl enable mariadb
4. sudo systemctl start mariadb

###Rubyをインストール
1. sudo yum install ruby ruby-devel -y

##GitHubクローン, Jenkins, Redmineの構築

###GitHubクローンの構築
1. wget https://github.com/takezoe/gitbucket/releases/download/3.1.1/gitbucket.war
2. sudo mv gitbucket.war /var/lib/tomcat/webapps
3. echo "<Location /gitbucket>
   ProxyPass ajp://localhost:8009/gitbucket
   </Location>
   " | sudo tee /etc/httpd/conf.d/gitbucket.conf

###Jenkinsの構築
1. wget http://mirrors.jenkins-ci.org/war/latest/jenkins.war
2. sudo mv jenkins.war /var/lib/tomcat/webapps
3. echo "<Location /jenkins>
ProxyPass ajp://localhost:8009/jenkins
</Location>
" | sudo tee /etc/httpd/conf.d/jenkins.conf

###Redmineの構築
1. sudo yum groupinstall "Development Tools" -y
2. sudo yum install openssl-devel readline-devel zlib-devel curl-devel libyaml-devel libffi-devel -y
3. sudo yum -y install ImageMagick ImageMagick-devel ipa-pgothic-fonts
4. sudo gem install bundler --no-rdoc --no-ri
5. mysql -uroot
6. create database db_redmine default character set utf8;
7. exit
8. wget http://www.redmine.org/releases/redmine-3.0.0.tar.gz
9. tar xvf redmine-3.0.0.tar.gz
10. sudo mv redmine-3.0.0 /var/lib/redmine
11. sudo vi /var/lib/redmine/config/database.yml
production:
adapter: mysql2
database: db_redmine
host: localhost
username: root
password: ""
encoding: utf8
12. sudo vi /var/lib/redmine/config/configuration.yml
production:
email_delivery:
delivery_method: :smtp
smtp_settings:
address: "localhost"
port: 25
domain: 'example.com'

rmagick_font_path: /usr/share/fonts/ipa-pgothic/ipagp.ttf
13. cd /var/lib/redmine/
14. sudo yum install ImageMagick ImageMagick-devel ipa-pgothic-fonts -y
15. bundle install --without development test
16. bundle exec rake generate_secret_token
17. gem install passenger --no-rdoc --no-ri
18. sudo chmod 777 /home/vagrant
19. /home/vagrant/bin/passenger-install-apache2-module
20. /home/vagrant/bin/passenger-install-apache2-module --snippet | sudo tee /etc/httpd/conf.d/redmine.conf
21. sudo ln -s /var/lib/redmine/public /var/www/html/redmine
22. sudo service httpd configtest
23. sudo service httpd restart

##GitHubクローン, Jenkins, Redmineの連携

###GitHubクローンとJenkins
1. http://blacknd.com/linux-server/centos7-gitbucket-jenkins-auto-deploy/にそって行う

###GitHubクローンとRedmineの連携
1. http://symfoware.blog68.fc2.com/blog-entry-1694.htmlにそって行う

##セキュリティを元に戻す
1. sudo firewall-cmd --zone=public --add-service=http --permanent
2. sudo systemctl start firewalld

#完了！
