package jp.kotei.kenshu.soukoban.model;

public class Ball extends Model{
	private final static String NAME = "o";
	private final static int LAYER = 1;
	private final static boolean CAN_MOVE = true;
	private final static boolean IS_GOAL = false;
	private final static boolean IS_PLAYER = false;
	
	public Ball() {
		super(NAME, LAYER, CAN_MOVE, IS_GOAL, IS_PLAYER);
	}
}
