package jp.kotei.kenshu.soukoban.model;

public class Wall extends Model{
	private final static String NAME = "#";
	private final static int LAYER = 1;
	private final static boolean CAN_MOVE = false;
	private final static boolean IS_GOAL = false;
	private final static boolean IS_PLAYER = false;
	
	public Wall() {
		super(NAME, LAYER, CAN_MOVE, IS_GOAL, IS_PLAYER);
	}
}
