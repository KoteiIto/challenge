package jp.kotei.kenshu.soukoban.model;

public class Player extends Model{
	private final static String NAME = "p";
	private final static int LAYER = 1;
	private final static boolean CAN_MOVE = true;
	private final static boolean IS_GOAL = false;
	private final static boolean IS_PLAYER = true;
	
	public Player() {
		super(NAME, LAYER, CAN_MOVE, IS_GOAL, IS_PLAYER);
	}

}
