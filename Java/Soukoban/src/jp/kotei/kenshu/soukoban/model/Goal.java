package jp.kotei.kenshu.soukoban.model;

public class Goal extends Model{

	private final static String NAME = ".";
	private final static int LAYER = 0;
	private final static boolean CAN_MOVE = false;
	private final static boolean IS_GOAL = true;
	private final static boolean IS_PLAYER = false;
	
	public Goal() {
		super(NAME, LAYER, CAN_MOVE, IS_GOAL, IS_PLAYER);
	}

}
