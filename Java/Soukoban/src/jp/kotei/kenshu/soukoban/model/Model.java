package jp.kotei.kenshu.soukoban.model;

/**
 * ゲーム内の各モデルを汎化させた抽象クラス
 * @author kotei
 *
 */
public abstract class Model implements Cloneable{
	private String name;
	private int layer;
	private boolean canMove;
	private boolean isGoal;
	private boolean isPlayer;
		
	public Model(String name, int layer, 
			boolean canMove, boolean isGoal, boolean isPlayer) {
		this.name = name;
		this.layer = layer;
		this.canMove = canMove;
		this.isGoal = isGoal;
		this.isPlayer = isPlayer;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public boolean isCanMove() {
		return canMove;
	}

	public void setCanMove(boolean canMove) {
		this.canMove = canMove;
	}

	public boolean isGoal() {
		return isGoal;
	}

	public void setGoal(boolean isGoal) {
		this.isGoal = isGoal;
	}

	public boolean isPlayer() {
		return isPlayer;
	}

	public void setPlayer(boolean isPlayer) {
		this.isPlayer = isPlayer;
	}
	
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
}
