package jp.kotei.kenshu.soukoban.model;

public class Properties {
	private long timeLimit;
	private int turnLimit;
	
	public long getTimeLimit() {
		return timeLimit;
	}
	public void setTimeLimit(long timeLimit) {
		this.timeLimit = timeLimit;
	}
	public int getTurnLimit() {
		return turnLimit;
	}
	public void setTurnLimit(int turnLimit) {
		this.turnLimit = turnLimit;
	}
	
}
