package jp.kotei.kenshu.soukoban.manager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jp.kotei.kenshu.soukoban.model.Model;
import jp.kotei.kenshu.soukoban.stage.Stage;
import jp.kotei.kenshu.soukoban.util.Vector3;

/**
 * ステージを管理するマネージャクラス
 * @author kotei
 *
 */
public class StageManager {
	private static StageManager stageManager = new StageManager();
	private List<Stage> stageList = new ArrayList<>();
	private List<Model> modelList;
	private int stageIndex = 0;
	private int turnLimit;
	
	private StageManager(){}
	
	public static StageManager getManager(){
		return stageManager;
	}
	
	/**
	 * ゲームで使うモデルをセットする
	 * @param modelList
	 */
	public void setModelList(List<Model> modelList){
		this.modelList = modelList;
	}
	
	public List<Model> getModelList(){
		return modelList;
	}

	/**
	 * モデルリストからモデルを生成する
	 * @param name
	 * @return
	 */
	private Model initModel(String name) {
		for(Model model : modelList){
			if(model.getName().equals(name)){
				return model;
			}
		}
		return null;
	}
	
	/**
	 * 指定位置にモデルをセットする
	 * @param position
	 * @param model
	 * @throws NullPointerException
	 */
	public void setModel(Vector3 position, Model model) throws NullPointerException{
		Stage stage = stageList.get(stageIndex);
		stage.getStageData().get(position.y).get(position.x).set(position.z, model);
	}
	
	/**
	 * 指定位置からモデルを取得する
	 * @param position
	 * @return
	 * @throws NullPointerException
	 */
	public Model getModel(Vector3 position) throws NullPointerException{
		Stage stage = stageList.get(stageIndex);
		return stage.getStageData().get(position.y).get(position.x).get(position.z);
	}
	
	/**
	 * ステージを作成する
	 * @param fileData
	 */
	public void createStage(List<List<String>> fileData){
		Stage stage = new Stage();
		List<List<List<Model>>> stageData = new ArrayList<>();
		List<List<Model>> horizontalStageDatal= new ArrayList<>();
		List<Model> layerStageData = new ArrayList<>();
		Model model;
		
		for(List<String> horizontalFileData : fileData){
			for(String name : horizontalFileData){
				layerStageData.add(null);
				layerStageData.add(null);
				model = initModel(name);
				if(model != null){
					layerStageData.add(model.getLayer(), model);
				}
				horizontalStageDatal.add(layerStageData);
				layerStageData = new ArrayList<>();
			}
			stageData.add(horizontalStageDatal);
			horizontalStageDatal = new ArrayList<>();
		}
		stage.setStageData(stageData);
		stageList.add(stage);
	}
	
	/**
	 * プレイヤーの位置を取得する
	 * @param player
	 * @return
	 */
	public Vector3 getPlayerPosition(Model player){
		Stage stage = stageList.get(stageIndex);
		List<List<List<Model>>> stageData = stage.getStageData();
		for(int y = 0; y < stageData.size(); y++){
			for(int x = 0; x < stageData.get(y).size(); x++){
				for(int z = 0; z < stageData.get(y).get(x).size(); z++){
					if(player == stageData.get(y).get(x).get(z)){
						return new Vector3(x,y,z);
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * ステージをディープコピーする
	 */
	public void cloneStage(){
		Stage stage = stageList.get(stageIndex);
		Stage newStage = new Stage();
		List<List<List<Model>>> newStageData = new ArrayList<>();
		List<List<Model>> newHorizontalList = new ArrayList<>();
		List<Model> newLayerList = new ArrayList<>();
		try {
			for(List<List<Model>> horizontalList : stage.getStageData()){
				for(List<Model> layerList : horizontalList){
					for(Model model : layerList){
						newLayerList.add(model);
					}
					newHorizontalList.add(newLayerList);
					newLayerList = new ArrayList<>();
				}
				newStageData.add(newHorizontalList);
				newHorizontalList = new ArrayList<>();
			}
			newStage.setStageData(newStageData);
			stageList.add(stageIndex,newStage);
			stageIndex++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * ステージを出力する
	 */
	public void printStage(){
		Stage stage = stageList.get(stageIndex);
		List<List<List<Model>>> stageData = stage.getStageData();

		for(List<List<Model>> horizontalList : stageData){
			for(List<Model> layerList : horizontalList){
				printModel(layerList);
			}
			System.out.println();
		}
	}
	
	/**
	 * モデルを出力する
	 * @param layerList
	 */
	private void printModel(List<Model> layerList){
		if(layerList.get(1) != null && layerList.get(0) != null){
			if(layerList.get(1).getName() == "o" || layerList.get(0).getName() != "."){
				System.out.print(layerList.get(1).getName().toUpperCase());
				return;
			}
		}
		if(layerList.get(1) != null){
			System.out.print(layerList.get(1).getName());
			return;
		}
		
		if(layerList.get(0) != null){
			System.out.print(layerList.get(0).getName());
			return;
		}
		
		System.out.print(" ");		
	}

	/**
	 * クリアしたかどうか判定する
	 * @param goal
	 * @param ball
	 * @return
	 */
	public boolean checkClear(Model goal, Model ball) {
		Stage stage = stageList.get(stageIndex);
		List<List<List<Model>>> stageData = stage.getStageData();
		for(List<List<Model>> horizontalList : stageData){
			for(List<Model> layerList : horizontalList){
				if(layerList.get(goal.getLayer()) == goal 
						&& layerList.get(ball.getLayer()) != ball){
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 巻き戻しを行う
	 * @return
	 */
	public boolean undo() {
		if(stageIndex - 1 >= 0 ){
			stageIndex--;
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * ゲームリセット
	 */
	public void reset(){
		while(undo()){
			
		}
	}
	
	/**
	 * ターン数を返却する
	 * @return
	 */
	public int getStageIndex(){
		return stageIndex;
	}
	
	/**
	 * ターンのリミットを返却する
	 * @param turnLimit
	 */
	public void setTurnLimit(int turnLimit){
		this.turnLimit = turnLimit;
	}
	
	/**
	 * 残りターン数を返却する
	 * @return
	 */
	public int getRemaingTurn(){
		return turnLimit - stageIndex;
	}

	/**
	 * ステージのファイル出力用のデータを返却する
	 * @return
	 */
	public List<List<String>> getStageData() {
		List<List<List<Model>>> stageData = stageList.get(stageIndex).getStageData();
		List<List<String>> stageDataList = new ArrayList<>();
		List<String> horizontalData = new ArrayList<>();
		
		for(List<List<Model>> horizontalList : stageData){
			for(List<Model> layerList : horizontalList){
				if(layerList.get(1) == null){
					if(layerList.get(0) == null){
						horizontalData.add(" ");
					}else{
						horizontalData.add(layerList.get(0).getName());
					}
				}else{
					horizontalData.add(layerList.get(1).getName());
				}
			}
			stageDataList.add(horizontalData);
			horizontalData = new ArrayList<>();
		}
		return stageDataList;
	}
	
}
