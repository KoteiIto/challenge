package jp.kotei.kenshu.soukoban.manager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jp.kotei.kenshu.soukoban.model.Properties;

/**
 * 入出力を管理するマネージャクラス
 * @author kotei
 *
 */
public class IOManager {
	private static IOManager ioManager = new IOManager();
	private String MESSAGE_GET_COMMAND = "コマンドを入力してください。";
	private String MESSAGE_COMMAND_HELP_MOVE = "w:前, s:後, a:左, d:右";
	private String MESSAGE_COMMAND_HELP_OTHER = "u:Undo, r:Reset, p:Pause + Save";
	private String MESSAGE_SELECT_STAGE = "プレイするステージの番号を選択してください。";
	private String MESSAGE_WRONG_COMMAND = "コマンドが正しくありません。";
	private final String MESSAGE_PAUSE = "保存する名前を入力してください。";
	
	private IOManager(){};
	
	public static IOManager getManager(){
		return ioManager;
	}
			
	/**
	 * ステージの生成用データをロードする
	 * @param path
	 * @return
	 */
	public List<List<String>> loadStage(String path){
		List<List<String>> fileData = new ArrayList<List<String>>();
		
		try (
			FileReader fr = new FileReader(path);
			BufferedReader br = new BufferedReader(fr);
		){
            //読み込んだファイルを１行ずつ処理する
            String line;
            String[] figures;
            List<String> list = new ArrayList<String>();
            while ((line = br.readLine()) != null) {
                //区切り文字","で分割する
            	figures = line.split(",");
            	list = (List<String>)Arrays.asList(figures);
            	fileData.add(list);
            }
            return fileData;

        } catch (IOException e) {
            //例外発生時処理
            e.printStackTrace();
            return null;
        }
	}
	
	/**
	 * 制限時間などのゲームプロパティをロードする
	 * @param path
	 * @return
	 */
	public Properties loadProperties(String path){
		Properties properties = new Properties();
		try (
				FileReader fr = new FileReader(path);
				BufferedReader br = new BufferedReader(fr);
			){
	            //読み込んだファイルを１行ずつ処理する
	            String line;
	            String[] strings = {} ;
	            while ((line = br.readLine()) != null) {
		            strings = line.split(",");
		            if(strings[0].equals("timeLimit")){
		            	properties.setTimeLimit(Long.parseLong(strings[1]));
		            }           
		            if(strings[0].equals("turnLimit")){
		            	properties.setTurnLimit(Integer.parseInt(strings[1]));
		            }
	            }
	            return properties;
	            
	        } catch (IOException e){
	        	e.printStackTrace();
	        	return null;
	        }
	}
	
	/**
	 * ディレクトリのファイル一覧から、保存したステージ一覧を出力する
	 * @param path
	 * @return
	 */
	public List<String> getStageNameList(String path){
		List<String> stageNameList = new ArrayList<>();
		File dir = new File(path);
		File[] files = dir.listFiles();
		for(int i = 0; i < files.length; i++){
			String fileFullName = files[i].toString();
			String[] slashSplits = fileFullName.split("/");
			String fileName = slashSplits[slashSplits.length - 1];
			stageNameList.add(fileName);
		}
		return stageNameList;
	}
	
	/**
	 * コマンドの入力を受け取る
	 * @param commands
	 * @return
	 */
	public String getCommand(String[] commands){
		String command = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		do{
			try{
				System.out.println(MESSAGE_GET_COMMAND);
				System.out.println(MESSAGE_COMMAND_HELP_MOVE);
				System.out.println(MESSAGE_COMMAND_HELP_OTHER);
				command = br.readLine();
			} catch(IOException e){
				e.printStackTrace();
			}
		}while(!validation(command, commands));
		return command;
	}

	/**
	 * ユーザからステージの選択を受け取る
	 * @param length
	 * @return
	 */
	public int getStageIndex(int length){
		String stage = "";
		int stageIndex = -1;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		do{
			try{
				System.out.println(MESSAGE_SELECT_STAGE);
				stage = br.readLine();
				stageIndex = Integer.parseInt(stage);
			} catch(Exception e){
				System.out.println("正しく選択してください");
				continue;
			}
		}while(stageIndex < 0 || length <= stageIndex);
		return stageIndex;
	}
	
	/**
	 * コマンドの入力チェック
	 * @param command
	 * @param commands
	 * @return
	 */
	private boolean validation(String command, String[] commands) {
		for(int i = 0; i < commands.length; i++){
			if(commands[i].equals(command)){
				return true;
			}
		}
		System.out.println(MESSAGE_WRONG_COMMAND);
		return false;
	}

	/**
	 * ユーザから保存する時の名前を受け取る
	 * @return
	 */
	public String getSaveName() {
		String saveName = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		do{
			try{
				System.out.println(MESSAGE_PAUSE);
				saveName = br.readLine();
			} catch(IOException e){
				e.printStackTrace();
			}
		}while(saveName.length() == 0);
		return saveName;
	}

	/**
	 * ステージをファイルに保存する
	 * @param path
	 * @param stageData
	 */
	public void saveStageData(String path, List<List<String>> stageData) {
		File file = new File(path);
		StringBuilder builder = new StringBuilder();

		try(
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file)));
		) {
			for(List<String> horizontalList : stageData){
				for(String name : horizontalList){
					builder.append(name);
					builder.append(',');
				}
				writer.println(builder.toString());
				builder = new StringBuilder();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * プロパティをファイルに保存する
	 * @param path
	 * @param properties
	 */
	public void saveProperties(String path, Properties properties) {
		File file = new File(path);
		try(
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file)));
		) {
			writer.println("timeLimit" + "," + properties.getTimeLimit());
			writer.println("turnLimit" + "," + properties.getTurnLimit());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
