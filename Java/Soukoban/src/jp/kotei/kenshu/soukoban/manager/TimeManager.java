package jp.kotei.kenshu.soukoban.manager;

/**
 * 時間を管理するマネージャクラス
 * @author kotei
 *
 */
public class TimeManager {
	
	private static TimeManager timeManager = new TimeManager();
	
	private long start;
	private long pauseStart;
	private long timeLimit;
	
	private TimeManager(){}
	
	public static TimeManager getManager(){
		return timeManager;
	}
	
	public void setTimeLimit(Long timeLimit){
		this.timeLimit = timeLimit;
	}
	
	/**
	 * タイマーをスタートさせる
	 */
	public void timerStart(){
		start = System.currentTimeMillis();
	}
	
	/**
	 * 残り時間を返却する
	 * @return
	 */
	public long getRemainingTime(){
		return timeLimit - (System.currentTimeMillis() - start) / 1000;
	}
	
	/**
	 * タイマーを止める
	 */
	public void pauseStart(){
		pauseStart = System.currentTimeMillis();
	}
	
	/**
	 * タイマーを再開する
	 */
	public void pauseStop(){
		long pauseTime = (System.currentTimeMillis() - pauseStart) / 1000;
		timeLimit += pauseTime;
	}
	
}
