package jp.kotei.kenshu.soukoban.stage;

import java.util.ArrayList;
import java.util.List;

import jp.kotei.kenshu.soukoban.model.Model;

public class Stage implements Cloneable{
	private List<List<List<Model>>> stageData;
	
	public List<List<List<Model>>> getStageData() {
		return stageData;
	}

	public void setStageData(List<List<List<Model>>> stageData) {
		this.stageData = stageData;
	}

	public Stage(){
		 stageData = new ArrayList<>();
	}	
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
}
