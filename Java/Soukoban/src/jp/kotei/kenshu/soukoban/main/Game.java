package jp.kotei.kenshu.soukoban.main;

import java.util.ArrayList;
import java.util.List;

import jp.kotei.kenshu.soukoban.manager.IOManager;
import jp.kotei.kenshu.soukoban.manager.StageManager;
import jp.kotei.kenshu.soukoban.manager.TimeManager;
import jp.kotei.kenshu.soukoban.model.Ball;
import jp.kotei.kenshu.soukoban.model.Goal;
import jp.kotei.kenshu.soukoban.model.Model;
import jp.kotei.kenshu.soukoban.model.Player;
import jp.kotei.kenshu.soukoban.model.Properties;
import jp.kotei.kenshu.soukoban.model.Wall;
import jp.kotei.kenshu.soukoban.util.Vector3;
/**
 * ゲームクラス
 * @author kotei
 *
 */
public class Game {
	//メッセージ用
	private final String MESSAGE_CLEAR = "クリアしました。";
	private final String MESSAGE_TIMEOVER = "制限時間を超えました。";
	private final String MESSAGE_TURNOVER = "制限ターンを超えました。";
	private final String MESSAGE_GAMEOVER = "ゲームオーバーです。";
	private final String MESSAGE_GAMEPAUSED = "ゲームを保存しました。お疲れ様です。";
	
	//コマンドのバリデーション用
	private final String[] commands = {"w","a","d","s","u","r","p"};
	
	//各種マネージャ
	private IOManager ioManager;
	private StageManager stageManager;
	private TimeManager timeManager;
	
	//ゲームに登場する各モデルのリスト
	private List<Model> modelList;
	//ファイルへのパス
	private String mapPath;
	private String propertiesPath;
	private String fileName;
	
	public static void main(String[] args) {
		Game game = new Game();
		game.initiate();
		game.loadContents();
		game.run();
	}
	
	/**
	 * ゲーム初期化
	 */
	public void initiate(){
		System.out.println("\n\n\n");

		ioManager = IOManager.getManager();
		
		stageManager = StageManager.getManager();	
		modelList = new ArrayList<>();
		modelList.add(new Player());
		modelList.add(new Goal());
		modelList.add(new Ball());
		modelList.add(new Wall());
		stageManager.setModelList(modelList);
		
		timeManager = TimeManager.getManager();
		
		fileName = "map1.csv";
		mapPath = "maps/";
		propertiesPath ="property/";
		
	}
	
	/**
	 * データのロード
	 */
	public void loadContents(){
		loadStage();
	}

	/**
	 * マップデータとプロパティの読み込み
	 */
	private void loadStage() {
		List<String> fileNameList = ioManager.getStageNameList(mapPath);
		for(int i = 0; i < fileNameList.size();i++){
			System.out.println(i + " : " + fileNameList.get(i).replaceAll(".csv", ""));
		}
		int stageIndex = ioManager.getStageIndex(fileNameList.size());
		fileName = fileNameList.get(stageIndex);
		Properties properties = ioManager.loadProperties(propertiesPath + fileName);
		stageManager.setTurnLimit(properties.getTurnLimit());
		timeManager.setTimeLimit(properties.getTimeLimit());
		List<List<String>> fileData = ioManager.loadStage(mapPath + fileName);
		
		stageManager.createStage(fileData);
	}

	/**
	 * ゲームループ
	 */
	public void run(){
		String command;
		Vector3 playerPosition;
		
		timeManager.timerStart();
		long remainingTime;
		int remainingTurn;
		boolean isGameOver = false;
		do{
			remainingTime = timeManager.getRemainingTime();
			remainingTurn = stageManager.getRemaingTurn();
			if(remainingTime < 0 ){
				System.out.println(MESSAGE_TIMEOVER);
				isGameOver = true;
				break;
			}
			if(remainingTurn < 0){
				System.out.println(MESSAGE_TURNOVER);
				isGameOver = true;
				break;
			}
			System.out.println("残り時間: " + remainingTime);
			System.out.println("残りターン: " + remainingTurn);

			stageManager.printStage();
			command = ioManager.getCommand(commands);
			playerPosition = stageManager.getPlayerPosition(modelList.get(0));
			
			doCommand(playerPosition, command);			
		}while(!stageManager.checkClear(modelList.get(1), modelList.get(2)));
		if(isGameOver){
			System.out.println(MESSAGE_GAMEOVER);
		}else{
			stageManager.printStage();
			System.out.println(MESSAGE_CLEAR);
		}
		Game.main(null);
	}

	/**
	 * コマンドを実行する
	 * @param playerPosition
	 * @param command
	 */
	private void doCommand(Vector3 playerPosition, String command) {
		switch(command){
		case "w" :
			move(playerPosition, new Vector3(0, -1, 0));
			break;
		case "s" :
			move(playerPosition, new Vector3(0, 1, 0));
			break;
		case "a" :
			move(playerPosition, new Vector3(-1, 0, 0));
			break;
		case "d" :
			move(playerPosition, new Vector3(1, 0, 0));
			break;
		case "r" :
			reset();
			break;
		case "u" :
			undo();
			break;
		case "p" :
			pause();
			break;
		}
		
	}

	/**
	 * やり直し
	 */
	private void undo() {
		stageManager.undo();
	}

	/**
	 * １ターン目に戻る
	 */
	private void reset() {
		stageManager.reset();
	}

	/**
	 * 指定方向へプレイヤーを移動する
	 * @param prevPlayerPosition
	 * @param direction
	 */
	private void move(Vector3 prevPlayerPosition, Vector3 direction) {
		stageManager.cloneStage();
		
		Model player = stageManager.getModel(prevPlayerPosition);
		Vector3 nextPlayerPosition = prevPlayerPosition.sum(direction);
		Model nextModel = stageManager.getModel(nextPlayerPosition);
		
		if(nextModel == null){
			replace(prevPlayerPosition, nextPlayerPosition, player, nextModel);
			return;
		}
		
		if(nextModel.isCanMove()){
			Vector3 nextModelPosition = nextPlayerPosition.sum(direction);
			Model nextNextModel = stageManager.getModel(nextModelPosition);
			if(nextNextModel == null){
				replace(nextPlayerPosition, nextModelPosition, nextModel, nextNextModel);
				replace(prevPlayerPosition, nextPlayerPosition, player, nextNextModel);
				return;
			}
		}
		
	}
	
	/**
	 * オブジェクトの配置を交換する
	 * @param prevPosition
	 * @param nextPosition
	 * @param prevModel
	 * @param nextModel
	 */
	private void replace(Vector3 prevPosition, Vector3 nextPosition, 
			Model prevModel, Model nextModel){
		stageManager.setModel(nextPosition, prevModel);
		stageManager.setModel(prevPosition, nextModel);
		
	}
	
	/**
	 * ゲームを中断し、保存する
	 */
	private void pause(){
		timeManager.pauseStart();
		String saveName = ioManager.getSaveName();
		timeManager.pauseStop();
		Properties properties = new Properties();
		properties.setTimeLimit(timeManager.getRemainingTime());
		properties.setTurnLimit(stageManager.getRemaingTurn());
		
		List<List<String>> stageData = stageManager.getStageData();
		
		ioManager.saveStageData(mapPath + saveName + ".csv", stageData);
		ioManager.saveProperties(propertiesPath + saveName + ".csv", properties);
		
		System.out.println(MESSAGE_GAMEPAUSED);
		Game.main(null);
	}
}
