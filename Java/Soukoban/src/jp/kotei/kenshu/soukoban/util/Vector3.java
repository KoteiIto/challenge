package jp.kotei.kenshu.soukoban.util;

/**
 * ３次元のベクトルを扱うクラス
 * @author a13888
 *
 */
public class Vector3 {
	private final int DEFAULT_X = 0;
	private final int DEFAULT_Y = 0;
	private final int DEFAULT_Z = 0; 
	
	public int x;
	public int y;
	public int z;
	
	public Vector3(){
		this.x = DEFAULT_X;
		this.y = DEFAULT_Y;
		this.z = DEFAULT_Z;
	}
	
	public Vector3(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3 sum(Vector3 vector){
		int x = vector.x;
		int y = vector.y;
		int z = vector.z;
		Vector3 result = new Vector3(this.x + x, this.y + y, this.z + z);
		return result;
	}
}
